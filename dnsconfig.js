// Providers:

var REG_NONE = NewRegistrar('none', 'NONE');
var DNS_CLOUDFLARE = NewDnsProvider('cloudflare', 'CLOUDFLAREAPI');

DEFAULTS(
    CF_PROXY_DEFAULT_OFF // turn proxy off when not specified otherwise
);

function GoogleEmail() {
    return [
        MX('@', 1, 'ASPMX.L.GOOGLE.COM.'),
        MX('@', 5, 'ALT1.ASPMX.L.GOOGLE.COM.'),
        MX('@', 5, 'ALT2.ASPMX.L.GOOGLE.COM.'),
        MX('@', 10, 'ALT3.ASPMX.L.GOOGLE.COM.'),
        MX('@', 10, 'ALT4.ASPMX.L.GOOGLE.COM.')
    ];
}

// Domains:

D('schaakclublievegem.be', REG_NONE, DnsProvider(DNS_CLOUDFLARE),
    A('@', '188.166.48.83', CF_PROXY_ON),
    AAAA('@', '2a03:b0c0:2:d0::d0d:1', CF_PROXY_ON),
    CNAME('www', '@', CF_PROXY_ON),
    A('dev','188.166.48.83',CF_PROXY_ON)
);

