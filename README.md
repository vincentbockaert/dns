# Automated CloudFlare Zone Record managed via GitLab CI/CD & DnsControl

Thanks to the great work by CloudFlare (for their amazing API and granular permission scheme) as well as the great work by StackExchange on DnsControl,
(I'm) we? 🤔 are able to 🚗 automate DNS changes and keep it all version controlled via my favourite toolbox 🛠️ CI/CD system: GitLab CI/CD.

## Pre-requisites:

- Create CloudFlare API Token 
    - ![Image on how to create the cloudflare API Token](https://i.imgur.com/KTjEX6o.png)
- Very basic knowledge of GitLab CI/CD
- Barebones knowledge of records (your grandmother could probably do it after 5 minute intro into what A, AAA & CNAME mean)
- Knowledge of `git add`, `git commit`, `git push`

## Let's do it

Well ... you don't really need to do anything :D.
Just check out the repository and make sure add the CloudFlare API Token to the CI/CD Variables.
See image below on where to find this option:

![](https://i.imgur.com/qPBHLFq.png)

Make sure your domain is already visible in CloudFlare _(AKA using the CloudFlare's nameservers, cloudflare doesn't have to be your registrar)_.
Then simply add the domain in `dsnconfig.js`, see for example the schaakclublievegem.be domain:

    D('schaakclublievegem.be', REG_NONE, DnsProvider(DNS_CLOUDFLARE),
        A('@', '188.166.48.83', CF_PROXY_ON), // don't use cloudflare proxying
        AAAA('@', '2a03:b0c0:2:d0::d0d:1', CF_PROXY_ON),
        CNAME('www', '@', CF_PROXY_ON)
    );

_note: the CF\_PROXY\_ON this isn't needed if you set the default to PROXY\_OFF but I add it for clarity_